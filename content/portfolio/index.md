+++
date = 2020-04-18
title = "Portfolio"
template = "page.html"
description = "@import_bakeryのポートフォリオ"
+++

## 目次

* [各種リンク](#ge-zhong-rinku)
* [作ったもの](#zuo-tutamono)
  * [Ghash](#ghash)
  * [Ghash Wasm](#ghash-wasm)
  * [TOSS](#toss)
  * [Typed Numeric](#typed-numeric)
  * [VMusic Studio](#vmusic-studio)
  * [Unofficial-API](#unofficial-api)
* [参加したイベント等](#can-jia-sitaibentodeng)

## 各種リンク

GitLabをメインで使ってます。

* [Twitter](https://twitter.com/import_bakery)
* [GitLab](https://gitlab.com/glatan)
* [GitHub](https://github.com/glatan)

<section>
<div class="works">

## 作ったもの

<section class="work">

## Ghash

### [Repository](https://gitlab.com/glatan/ghash)

### About

いろんなHashアルゴリズムをRustで実装したもの。

### 使用技術

Rust

### ひとこと

SHA3の候補だったアルゴリズムなど，名前を聞くことのないアルゴリズムについて知ることができて結構楽しい。

</section>
<section class="work">

## Ghash Wasm

### [Repository](https://gitlab.com/glatan/ghash-wasm)

### About

[Ghash](#ghash)をYewを使ってWebAssemblyなWebアプリケーションにしたもの。[**このページ**](https://ghash.glatan.vercel.app)に埋め込んである。

### 使用技術

Rust, WebAssembly, [wasm-pack](https://github.com/rustwasm/wasm-pack), [Yew](https://github.com/yewstack/yew)

### ひとこと

React.jsとかVue.jsみたいなフレームワークもElmも使ったことないから比較はできないけど，Yewで置き換えることができるものはだいぶ限られるんじゃないかと思った。ゲームとか仮想通貨マイニング，あと動画みたいな大きなファイルの加工あたりでなら使えるかも?

</section>
<section class="work">

## TOSS

### Repository(非公開)

### About

以下PDFの1ページ目を参照  
[PDF](http://www.procon.gr.jp/wp-content/uploads//2017/04/28Kadai.pdf)

### 使用技術

Node.js, MariaDB

### ひとこと

初めてまともにプログラムを書いたプロダクトで，データベース周りを担当した。この少し前まで(C言語にて)arrayという変数名にすれば配列になると思い込んでいたことを考えると，このプロジェクトへの参加で結構成長できたんじゃないかと思う。

</section>
<section class="work">

## Typed Numeric

### [Repository](https://github.com/glatan/typed_numeric_deno)

### About

TypeScriptで実装した数値型ライブラリ

### 使用技術

Deno, TypeScript

### ひとこと

作ったはいいものの、どう考えても多用するとパフォーマンス悪化を招くので、Rustのコードを呼び出せるっぽいDeno Pluginを利用した実装に移行するのがいいかもしれない。

</section>
<section class="work">

## VMusic Studio

### Repository(非公開)

### About

VR空間で楽器を演奏できる作品。Oculus Riftでのみ動作確認済み。

[Twitterモーメント](https://twitter.com/i/moments/1201335120826777601)

### 使用技術

Unity, C#, Blender

### ひとこと

ただただVR空間で楽器を演奏したくて，保育園児時代の記憶を思い出しながら作った。借り物のOculus Riftでの制作でいろいろ辛かったので，自分用のOculus Questを入手して開発を続行したい。

</section>
<section class="work">

## Unofficial-API

### [Repository](https://gitlab.com/glatan/unofficial-api)

### About

津山高専の授業変更情報をスクレイピングしてJSONで返すやつ。2020年の中国地区コンピュータフェスティバルの作品の一部です。

### 使用技術

Rust

### ひとこと

過去のものを遡れば遡るほど新しい表記が出てくる中で、きちんとスクレイピングするのはなかなか厳しかった。あとはじめてAPIサーバーとHTMLのスクレイピングをできていい経験になった。

</section>
</div>
</section>

## 参加したイベント等

* 2016年 | Hack U 2016 Osaka, チーム名: 大吟醸
* 2017年 | Hack Day 2017, チーム名: 大吟醸
* 2017年 | 中国地区コンピュータフェスティバル
* 2017年 | 高専プロコン第28回大会課題部門, テーマ名: TOSS
* 2018年 | 高専プロコン第29回大会課題部門, テーマ名: NeXcury, 特別賞
* 2018年 | 中国地区コンピュータフェスティバル
* 2018年 | 未踏ジュニア2018, プロジェクト名: TouchBuy
* 2019年 | エイチーム1Dayインターン(ゲームプログラマーコース)
* 2019年 | 中国地区コンピュータフェスティバル
* 2019年 | CA Tech Challenge Intel NUCで始めるおうちKubernetesクラスタで自宅サーバ入門
* 2020年 | 中国地区コンピュータフェスティバル
