+++
title = "デスクトップの壁紙をBingのそれにするツールを作った"
date = 2019-12-14
description = "デスクトップの壁紙をBingのそれにするツールを作った話"
+++

先駆者は結構いますが自分でも作ってみたので記事にしました。

<!-- more -->

## 追記(2020-02-02)

### 変更点

* 壁紙をセットする部分をGoのコード内に埋め込みました。
* GNOMEに対応しました。

### 壁紙をセットする部分をGoのコード内に埋め込んだ

この記事を書いた段階では、macOSの壁紙を変更する部分のコードは.applescriptファイルにありましたが、それをGoのコード内部に埋め込みました。
AppleScript部分も短くなってスッキリ。

ただ、MBPがちょうど手元にないタイミングで変更をしたので動作は未確認。

### GNOMEに対応

[neofetchのコード](https://github.com/dylanaraps/neofetch/blob/master/neofetch#L1598-L1698)を参考にGNOMEに対応しました。

手元のGNOME環境で動作確認済みです。

{{ video(src="./gnome.webm", type="video/webm") }}

## 画像のダウンロード

これはサクッと実装できた。

トップページのHTML内にある，以下のようなmetaタグがその日の壁紙を参照しているようです(${IMAGE_ID}部分が日替わり)。で，${IMAGE_ID}直後の"**tmb**"の部分を1920x1080にすると1920×1080，UHDにすると大きくてきれいな画像を返すようです。[*1](#ref1)

```html
<meta property="og:image" content="https://www.bing.com/th?id=OHR.${IMAGE_ID}_tmb.jpg&amp;rf=" />
```

これを元にGolangで実装してみたのが以下のリポジトリにあります。

[Repository](https://gitlab.com/glatan/wallpaper-from-bing)

## 壁紙の設定(macOS)

こっちがつらかった。

まず一つ目，動くけど変更反映のためにDockを再起動するせいで数秒間3本指スワイプができなくなるのが少し面倒。

```bash
sqlite3 ~/Library/Application\ Support/Dock/desktoppicture.db "update data set value = '${IMAGE_PATH}'" && killall Dock
```

2つ目，Dockの再起動なしに動いたが引数に画像へのパスを取るようにしたいのでこれはだめ。

```applescript
tell application "System Events"
    tell every desktop
        set picture to "/path/to/image/file"
    end tell
end tell
```

3つ目，引数に画像へのパスがあるときのみ動くようにしたかったのだが動かない。`set picture to wallpaperPath`の1行上に`display dialog wallpaperPath`と入れると，それは動いたのでわけがわからなくなった。

```applescript
on run argv
    if (count of argv) > 0 then
        set wallpaperPath to (item 1 of argv)
        tell application "System Events"
            tell every desktop
                display dialog wallpaperPath -- 動く
                set picture to wallpaperPath -- 動かない
            end tell
        end tell
    end if
end run
```

4つ目，ようやくちゃんと動いたのがこれ。Stack Overflowでいろんな人が"これはどうよ?"ってコメントしてた中の一つ。なんでこれでうまいこと動くようになったのかまるでわからないが，とりあえず動いたからヨシ！[*2](#ref2)

```applescript
on run argv
    if (count of argv) > 0 then
        set wallpaperPath to (item 1 of argv)
        tell application "System Events" to set picture of (reference to every desktop) to wallpaperPath
    end if
end run
```

##### 1. UHDを指定したときの画像サイズは3790×2131だったり5610×3156だったりとバラバラなので，リサイズ前の画像っぽい。 {#ref1}

##### 2. たしか[ここ](https://stackoverflow.com/questions/48778687/change-mac-wallpaper-through-applescript)の2つ目の回答を改変したやつのはず。 {#ref2}
