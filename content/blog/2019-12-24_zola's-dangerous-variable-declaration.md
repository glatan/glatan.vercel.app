+++
title = "Zolaの危険な変数宣言"
date = 2020-01-21
description = "Zolaの危険な変数宣言"
+++

つい先程新しく追加した[sandboxページ](../../sandbox)製作中，sandboxセクションを開いているのに，blogセクション以下のページが表示される現象に遭遇しました。

<!-- more -->

## 原因

宣言した変数がZolaの変数を上書きしていました。警告とかでないんですね。

下は私がほぼ全てのページ共通で利用するHTMLをまとめたファイルの一部です。ここではsection変数にblogセクションの情報を保存して，最近の記事一覧を生成しています。

```html
<h3>最近の記事</h3>
<nav>
    {% set section = get_section(path="blog/_index.md") %}
    {% for page in blog.pages %}
        <p><a href="{{ page.permalink }}">{{ page.title | safe }}</a></p>
    {% endfor %}
</nav>
```

**すべてのページで読み込まれるHTML内部に，Zolaの用意している変数を上書きするコードがあった**原因はこれです。すべてのページで読み込んでいるのだから当然sandboxページも対象で，そこで参照される変数はすでにblogのセクション情報で上書きされていました。これが原因でsandboxセクション内のsection変数が，blogのセクション情報に置き換わっていたのでした。

## 修正

以下のようにすることで修正できました。変数名を書き換えただけです。

```html
<h3>最近の記事</h3>
<nav>
    {% set blog = get_section(path="blog/_index.md") %}
    {% for page in blog.pages %}
        <p><a href="{{ page.permalink }}">{{ page.title | safe }}</a></p>
    {% endfor %}
</nav>
```

## 学び

Zolaの用意している変数は上書き可能です。頭の片隅にでも置いておきましょう。
