+++
title = "Antergosの遺産"
date = 2020-03-18
+++

Arch Linuxの派生ディストリビューションの中で、比較的知名度の高かったAntergosが、[昨年の5月にプロジェクトの終了を発表](https://web.archive.org/web/20190809064653/https://antergos.com/blog/antergos-linux-project-ends)。しばらくしてからプロジェクトが終了し、AntergosのGitHubリポジトリも全てアーカイブされた。プロジェクト終了からずいぶん経ち、今更感はあるが、その中の一部について紹介をしていきたいと思う。

<!-- more -->

## 目次

* [Antergosについて](#antergosnituite)
* [Cnchi](#cnchi)
* [Web Greeter](#web-greeter)

## Antergosについて

プロジェクトの歴史的なものは[Wikipedia](https://ja.wikipedia.org/wiki/Antergos)に書いてあるので省略する。

このディストリビューションはArch Linuxに以下の2つを付け足したもので、純粋なArch LinuxをGUIによって楽にインストールすることができた。そのため私はAntergosを**Arch Linuxのインストーラー**のようなものとして認識し利用していた。

* GTK+テーマやアイコン、AURヘルパーなどが含まれる独自のpacman用リポジトリ
* 独自のGUIインストーラー

人気のArch派生ディストリビューションのManjaroとの比較もしておく。ManjaroはArch Linuxからの派生の中ではトップクラスに知名度・人気の高いもので、フォーラムを見る限りコミュニティが非常に大きい。中身の話をしよう。Manjaroはリポジトリ名こそ同じだが中身の違う独自リポジトリを持っている。これはArch Linuxのリポジトリの一部パッケージを抽出、それらをManjaro側でも検証、その後自前のリポジトリに追加するという管理がなされており、Arch Linuxのパッケージリポジトリよりバージョンは少し遅れ気味(安定性はおそらく高い)で、数も少ないものになっている。そのためArch Linuxをベースにしつつも独自路線を行く、純粋なArch Linuxとは異なるディストリビューションだと言えるだろう。

AntergosとManjaro、どちらもArch Linuxをベースにし、GUIでの利用を前提に構築され、見た目も綺麗だ。だが、Antergosのほうが純粋なArch Linuxに近く、それを求めるユーザーを多く抱えていた。そのため、Antergosプロジェクトの終了が発表された後、有志がAntergosの後継を目指し[EndevourOS](https://endeavouros.com/)というディストリビューションの開発が始まった。私はまだ試せていないのだが、Antergosの開発が終了して途方に暮れていたユーザーはこれを使ってみるのもいいかもしれない。(Manjaroと同じくXfce推しに見える)

## Cnchi

Antergosで独自に開発・利用されていたCnchiというインストーラーがある。これはGUIでAntergosのインストールができるもので、ロケールの設定やパーティション編集、インストールするデスクトップ環境の選択などのよく見る機能は一通り実装されていた。Antergosプロジェクトの一環としての開発は終了したが、これまたArch Linux派生のReborn OSプロジェクトが利用しており、開発が継続しているようだ([リポジトリ](https://gitlab.com/reborn-os-team/cnchi-gnome-based))。

## Web Greeter

これはAntergosユーザー以外、おそらくArch Linux系以外を利用するユーザーにも知られているであろう、Antergosのプロダクトだ。

これはLightDMというディスプレイマネージャー用のグリーターで、Webkit を利用しており、その外見を変更するのにHTML, CSS, JavaScriptを利用できる。そのため、とてもLinuxのものとは思えないようなログイン画面を作ることができる。GitHubで[`lightdm-webkit2-greeter`](https://github.com/topics/lightdm-webkit2-greeter)や[`lightdm-theme`](https://github.com/topics/lightdm-theme)といったトピックで検索するとこのグリーター用のテーマがいくつか見つかるが、どれもあなたを驚かせるような見た目をしているだろう。

LightDMのグリーターは他にもあるが、ウェブページを作るようにログイン画面を作れるこのWeb Greeterは、特別凝ったデザインができるということで人気が高かった。そしてそれはAntergosプロジェクトが終了しても変わっていないだろう。開発が終了しリポジトリがアーカイブされてからもArch LinuxのCommunityリポジトリにパッケージがあり、更新されているテーマもある。しかし(私が見つけられていないだけかもしれないが)こちらは後継プロジェクトも、開発を継続している個人・団体もなく、今後本体や依存ライブラリに脆弱性が見つかったとしても更新されることはない。~~思い立った私が後継プロダクトを作るべきなのだろうか。~~

