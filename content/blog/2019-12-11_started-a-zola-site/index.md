+++
title = "Zolaに入門した"
date = 2019-12-11
description = "Zolaに入門した話"
+++

このサイトは[Zola](https://www.getzola.org/)で作っているのですが，日本語の情報がほとんどなく少し苦労しました。後でまた振り返る事ができるよう，メモを残しておきます。

<!-- more -->

## Tips

### もっと読むの実装

Zolaでは`page.summary`でそのページの概要を取得することができます。`page.summary`にはfront-matter(+++で囲まれたところ)と\<!-- more -->の間の要素が文字列として格納されています。下の例だと"**このサイト〜おきます。**" のところが`page.summary`になります。

```markdown
+++
title = "Zolaに入門した"
date = 2019-12-11
+++

このサイトは[Zola](https://www.getzola.org/)で作っているのですが，日本語の情報がほとんどなく少し苦労しました。後でまた振り返る事ができるよう，メモを残しておきます。

<!-- more -->

## Tips
```

これを利用すると以下のようにして実装できます。

```html
<h2><a href="{{ page.permalink }}">{{ page.title }}</a></h2>
{{ page.summary | safe }}
<a href="{{ page.permalink }}">続きを読む</a>
```

以下のように表示されるはずです。(これは私の書いたCSSが適用された状態)

{{ image(src="./more_read.png", alt="続きを読む") }}

注意点として，ページの頭にタイトルを入れていると，それも`page.summary`に含まれてしまいます。そのため，タイトルは上の例のように，templateファイル側で取得・表示するのがいいと思います。

### 最近の記事一覧の実装

Zolaの組み込み関数に，`get_section`というものがあります。実際に作ってみたものが下の例で，引数にそのセクションの_index.mdファイルへのパスを渡し，セクションの情報を取得。それをsection変数に格納し，その中のページ情報をfor文を回して取得・表示する。といった流れになっています。

```text:
# content/blog/post.md
+++
title = "Zolaに入門した"
+++
```

```html
<h3>最近の記事</h3>
<nav>
    {% set section = get_section(path="blog/_index.md") %}
    {% for page in section.pages %}
        <p><a href="{{ page.permalink }}">{{ page.title | safe }}</a></p>
    {% endfor %}
</nav>
```

このように表示されるはずです。

{{ image(src="./recently-post.png", alt="最近の記事") }}

## 参考になる資料

下記の資料はZolaでのサイト制作の助けになると思います。

### [公式ドキュメント](https://www.getzola.org/documentation/)

当然と言えば当然ですが大切。

### [Example](https://github.com/getzola/zola/blob/master/EXAMPLES.md)

Zolaで作成されたサイトとそのリポジトリがまとめられたページです。ソースコードと実際に作成されたサイトとを比較しながら見ていくのがいいと思います。

### [Tera](https://tera.netlify.com/docs/)

Zolaの内部で使用されているテンプレートエンジンのドキュメントです。Zolaで凝ったことをしたいならここを読みながら作業するのがいいと思います。この記事で利用していたfor文や変数のセット，といったものは全部Teraの機能です。
