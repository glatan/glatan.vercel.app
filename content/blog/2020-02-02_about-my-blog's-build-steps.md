+++
title = "このブログのビルドについて"
date = 2020-02-02
description = "このブログのビルドについて"
+++

[この記事](../started-a-zola-site/)にある通り、このブログはZolaを利用して生成されている。ただ実際に公開されているものは、Zolaの生成物にいろいろと手を加えたものなので、ここにその手順を書いていく。

ちなみにデプロイ先は[ZEIT Now](https://zeit.co/home)。

<!-- more -->

## ビルド手順

以下のような手順でこのブログはビルドされており、2~6はシェルスクリプトで実装している。

1. Zolaでビルド
2. [CSSをHTML内に埋め込む](#csswohtmlnei-bu-nimai-meip-mu)
3. [Minifyする](#minifysuru)
4. [PNGをWebPに変換](#pngwowebpnibian-huan)
5. [2~4によって不要になったファイルを削除](#bu-yao-ninatutahuairuwoxue-chu)
6. [gzipで圧縮](#gzipdeya-suo)

### CSSをHTML内部に埋め込む

ブログ内の全てのHTMLに対し以下のような手順で処理を行っている。

1. CSSファイルを探す
2. そのCSSファイルを参照しているHTMLを探す
3. CSSファイルをcatしてstyleタグ内部に埋め込んだものに置換

```bash
while IFS= read -r -d '' css; do
    _css_name=$(echo "${css}" | awk -F '/' '{print $NF}')
    while IFS= read -r -d '' html; do
        if grep -q "${_css_name}" "${html}"; then
            _before="<link rel=\"stylesheet\" href=.*${_css_name}\">"
            _after="<style>$(cat "${css}")</style>"
            sed -i -e "s|${_before}|${_after}|i" "${html}"
        fi
    done <  <(find public/ -name '*.html' -print0)
done <  <(find public/ -name '*.css' -print0)
```

### Minifyする

これは見たままで、全てのHTML・JS・XMLファイルを探してminifyしている。

minifyに利用しているのは[これ](https://github.com/tdewolff/minify)。npmに依存してしまうと`npm install`する必要が出てきて、ビルド時間が長くなりそうだったので、Go製かつバイナリのtarballが配布されているこれにした。

```bash
find public/ -type f -regex '.*\.\(html\|js\|xml\)$' -exec minify {} -o {} \;
```

### PNGをWebPに変換

手順は以下の通り。WebPへの変換には[これ](https://github.com/webmproject/libwebp)を使用。

1. favicon.png以外のPNGファイルをWebPに変換
2. HTML内部のPNGファイルへの参照をWebPのものに書き換え

```bash
# favicon以外のPNGファイルをWebPに変換
find public/ -name 'favicon.png' -prune -o -name "*.png" -exec bash -c 'file="$1"; cwebp -quiet -q 80 ${file} -o ${file%.png}.webp' _ {} \;
# HTML内部のPNGファイルへの参照をWebPのものに書き換え
while IFS= read -r -d '' png; do
    while IFS= read -r -d '' html; do
        if grep -q "${png}" "${html}"; then
            sed -i -e "s/${png}/${png%.png}.webp/g" "${html}"
        fi
    done <  <(find public/ -name '*.html' -print0)
done <  <(find public/ -name 'favicon.png' -prune -o -name '*.png' -printf '%f\0')
```

### 不要になったファイルを削除

これも見たままで、HTML内部への埋め込みで不要になったCSSと、WebPへの変換で不要になったPNGを削除している。

```bash
find public/ -name 'favicon.png' -prune -o -type f -regex '.*\.\(css\|png\)$' -exec rm {} \;
```

### gzipで圧縮

これも見たまま。WebPやWebMのようなすでに圧縮されているものは除いて、gzipで圧縮している。

```bash
find public/ -type f -regex '.*\.\(html\|js\|png\|wasm\|xml\)$' -exec bash -c 'gzip < ${1} > ${1}.gz' _ {} \;
```

## 最後に

現時点で静的サイトジェネレーターでの生成後にやってる処理が多いのはまぁ大丈夫。なのだが、[やってみたいこと](https://gitlab.com/glatan/glatan.now.sh/-/milestones/3)にあるDOM置換のようなことをやろうとすると、流石にシェルスクリプトでどうにかするのは厳しくなってくる。
そのため、そろそろ[yew](https://github.com/yewstack/yew)あたりを使って自分で静的サイトジェネレーターを作るべきな気がしている。

あとシェルスクリプトを書くなら[ShellCheck](https://github.com/koalaman/shellcheck/)はすごく便利なのでぜひ使ってみてほしい。VSCodeの拡張機能もあるしぜひ。
