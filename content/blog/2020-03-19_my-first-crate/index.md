+++
title = "バージョンを比較するクレートを公開した"
date = 2020-03-19
+++

いろいろな形式のバージョンを比較するのに、いい感じのクレートがなかったので自分で作って公開してみました。
ライセンスはApache-2.0 OR MIT、リポジトリは[ここ](https://gitlab.com/glatan/vercomp/)です。

<!-- more -->

## 内部の挙動

使い方は[crates.io](https://crates.io/crates/vercomp)や[docs.rs](https://docs.rs/vercomp/)を見てください。

### Version.number

Versionの数字部分は、連続した数字をひとかたまりとして、それを`Vec<u32>`に格納していってます。数字以外はスキップしているので区切り文字は"."でも"-"でもなんでも大丈夫です。

例) "0.1.3" => [0, 1, 3], "12-345-678" => [12, 345, 678], "01----2,.,.345" => [1, 2, 345]

### Version.stage

以下のものが実装されています。大文字小文字を問わず、alpha・beta・dev・nightly・rcが含まれていれば対応したものになり、該当するステージがなければStableの扱いになります。

* Stage::Alpha
* Stage::Beta
* Stage::Dev
* Stage::Nightly
* Stage::Rc
* Stage::Stable 
