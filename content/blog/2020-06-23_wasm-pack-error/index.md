+++
title = "wasm-packが高頻度でビルドに失敗する現象について"
date = 2020-06-23
+++

wasm-packでプロジェクトのビルドをする際、crate-typeにcdylibとrlibを指定しているのにもかかわらず、以下のエラーがかなりの頻度で出現し、ビルドが失敗する現象に遭遇した。wasm-packのバージョンは0.9.1。

```text
Error: crate-type must be cdylib to compile to wasm32-unknown-unknown. Add the following to your Cargo.toml file:

[lib]
crate-type = ["cdylib", "rlib"]
```

<!-- more -->

調べてみるとGitHubにIssueがあった。

> If you change name = "yew" in the Cargo [package] section to name = "WHATEVER_YOU_WANT" the bug disappears. So it seems that the problem is caused by the fact that the package name is equal to the name of a dependency.

[https://github.com/rustwasm/wasm-pack/issues/829](https://github.com/rustwasm/wasm-pack/issues/829)

どうやらpackage.nameと同じ名前の依存クレートがあると発生するらしい。この現象に遭遇したプロジェクトのCargo.tomlは以下(一部省略)で、確かにpackage.nameと同じ名前の依存クレートがある。

```toml
[package]
name = "ghash"

[lib]
crate-type = ["cdylib", "rlib"]

[dependencies]
yew = "0.16"
wasm-bindgen = "0.2"
ghash = { git = "https://gitlab.com/glatan/ghash", branch = "master" }
```

これのpackage.nameをghash-wasmに変更したところwasm-packがエラーを吐かなくなった。これを修正したプルリクエストはマージされているが、マイルストーンを見た感じだと、この修正を含んだ次のリリースにはまだ時間がかかりそうだ。

[https://github.com/rustwasm/wasm-pack/pull/830](https://github.com/rustwasm/wasm-pack/pull/830)
