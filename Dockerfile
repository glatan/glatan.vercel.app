FROM docker.io/debian:bullseye-slim

COPY scripts/setup.sh .

RUN apt update && \
    apt install -y curl && \
    ./setup.sh && \
    apt autoremove -y && \
    apt autoclean -y && \
    rm -rf /var/lib/apt/lists/*
