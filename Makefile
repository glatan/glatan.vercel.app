SHELL := $(shell which bash)
IMAGE_NAME := 'glatan.gitlab.io'
WORKDIR = '/workdir'
REPOSITORY_PATH = $(shell git rev-parse --show-toplevel)

.PHONY: default
default: up

.PHONY: p/build
p/build:
	@podman build -t ${IMAGE_NAME} .

.PHONY: init
init:
	@git submodule init
	@git submodule update
	@make ghash-wasm/init

.PHONY: up
up:
	-@podman run --name ${@} -p 1111:1111 -v .:${WORKDIR} -w ${WORKDIR} -it ${IMAGE_NAME} zola serve --interface 0.0.0.0
	@podman rm ${@}

.PHONY: build
build:
	-@podman run --name ${@} -v .:${WORKDIR} -w ${WORKDIR} -it ${IMAGE_NAME} zola build
	@podman rm ${@}

.PHONY: optimize
optimize: build
	-@podman run --name ${@} -v .:${WORKDIR} -w ${WORKDIR} -it ${IMAGE_NAME} scripts/optimize.sh
	@podman rm ${@}

.PHONY: run.bash
run.bash:
	-@podman run --name ${@} -v .:${WORKDIR} -w ${WORKDIR} -it ${IMAGE_NAME} bash
	@podman rm ${@}
