#!/usr/bin/env bash

# Install dependencies
scripts/setup.sh
if [[ $? -eq 1 ]]; then
    echo "Failed to install dependencies"
    exit 1
fi

# Build
./zola --version
./zola build
scripts/optimize.sh
if [[ $? -eq 1 ]]; then
    echo "Failed to optimize"
    exit 1
fi
