#!/usr/bin/env bash

# dependencies
MINIFY_VERSION='2.9.22'
LIBWEBP_VERSION='1.2.1'
ZOLA_VERSION='0.13.0'

curl -Ls "https://github.com/getzola/zola/releases/download/v${ZOLA_VERSION}/zola-v${ZOLA_VERSION}-x86_64-unknown-linux-gnu.tar.gz" | tar xz
curl -Ls "https://github.com/tdewolff/minify/releases/download/v${MINIFY_VERSION}/minify_linux_amd64.tar.gz" | tar xz
curl -Ls "http://downloads.webmproject.org/releases/webp/libwebp-${LIBWEBP_VERSION}-linux-x86-64.tar.gz" | tar xz

mv -f minify /usr/bin
mv -f "libwebp-${LIBWEBP_VERSION}-linux-x86-64/bin/cwebp" /usr/bin
if [[ ! $(command -v zola) ]]; then
    mv -f zola /usr/bin
fi

rm -rf "libwebp-${LIBWEBP_VERSION}-linux-x86-64"
