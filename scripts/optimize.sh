#!/usr/bin/env bash

set -u

# local CSS into HTML files.
_embed_local_css () {
    echo -e 'Embeding local CSS into HTML...'
    while IFS= read -r -d '' css; do
        _css_name=$(echo "${css}" | awk -F '/' '{print $NF}')
        while IFS= read -r -d '' html; do
            if grep -q "${_css_name}" "${html}"; then
                _before="<link rel=\"stylesheet\" href=.*${_css_name}\">"
                _after="<style>$(cat "${css}")</style>"
                sed -i -e "s|${_before}|${_after}|i" "${html}"
            fi
        done < <(find public/ -name '*.html' -print0)
    done < <(find public/ -name '*.css' -print0)
}

# Minify HTML, JS and XML files.
_minify () {
    echo -e 'Minifying HTML and JS...'
    find public/ -type f -regex '.*\.\(html\|js\|xml\)$' -exec minify {} -o {} \;
}

# Convert PNG to WebP.
_convert_image () {
    echo -e 'Converting PNG to WebP...'
    find public/ -name '*.png' -not -name 'favicon.png' -not -name 'og:image.png' -exec bash -c 'file="$1"; cwebp -quiet -q 80 ${file} -o ${file%.png}.webp' _ {} \;
    # Replace ".png" to ".webp" in HTML files.
    echo -e 'Replacing ".png" to ".webp"...'
    while IFS= read -r -d '' png_file; do
        while IFS= read -r -d '' refer; do
            if grep -q "${png_file}" "${refer}"; then
                sed -i -e "s/${png_file}/${png_file%.png}.webp/g" "${refer}"
            fi
        done <  <(find public/ -type f -regex '.*\.\(html\|xml\)$' -print0)
    done <  <(find public/ -name '*.png' -not -name 'favicon.png' -not -name 'og:image.png' -printf '%f\0')
}

# Remove PNG and CSS files.
_remove_cache () {
    echo -e 'Removing cache...'
    find public/ -type f -regex '.*\.\(css\|png\)$' -not -name 'favicon.png' -not -name 'og:image.png' -exec rm {} \;
}

echo -e "\e[1mBegin Optimizing\e[m"
_embed_local_css
_minify
_convert_image
_remove_cache
echo -e "\033[32;1mFinish Optimizing\e[m"
